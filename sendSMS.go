package mailer

import (
	"fmt"
	"io/ioutil"
	"strings"
)

// Send an sms with an internal template
func (m *mailer) SendSMS(n SMSNotifier) error {
	if n.TemplateName() == "" || n.To() == "" || n.From() == "" {
		return fmt.Errorf("template name, to and from must all be provided")
	}

	smsBody, err := readSMSTemplate(n.TemplateName(), n.TemplateKeyPairValues())
	err = m.smsClient.SendSMS(smsBody, n.From(), n.To())
	if err != nil {
		return fmt.Errorf("calling m.SendMail() TemplateName[%v], err: %v", n.TemplateName(), err)
	}

	return nil
}

func readSMSTemplate(templateName string, dynamicContent []*TemplateKeyPairValues) (string, error) {
	smsBody, err := readSMSTemplateFromFile(templateName)
	if err != nil {
		return "", fmt.Errorf("calling readSMSTemplateFromFile(), err:%v", err)
	}
	for _, templateData := range dynamicContent {
		smsBody = strings.ReplaceAll(smsBody, templateData.Name, templateData.Content)
	}

	return smsBody, nil
}

func readSMSTemplateFromFile(templateName string) (string, error) {
	filenameFullPath := fmt.Sprintf("%s/%s/%s", TemplatePath, "sms", templateName)
	rawFile, err := ioutil.ReadFile(filenameFullPath)
	if err != nil {
		return "", fmt.Errorf("template file not round or not readable, reading sms template from file(%v), err: %v", filenameFullPath, err)
	}

	return string(rawFile), nil
}
