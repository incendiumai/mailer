package mailer

const (
	ToMailOption      = MailOption("To")
	FromMailOption    = MailOption("From")
	SubjectMailOption = MailOption("Subject")
	BodyMailOption    = MailOption("Body")
	TemplateID        = MailOption("TemplateID")
)

type MailOption string

func (o MailOption) String() string {
	return string(o)
}

type MailOptions map[MailOption]string

func (m MailOptions) GetKey(option MailOption) string {
	return m[option]
}

type Attachment struct {
	Path  string
	Bytes []byte
}

type TemplateNotifier interface {
	Notifier
	TemplateKeyPairValues() []*TemplateKeyPairValues
}

type Notifier interface {
	TemplateName() string
	TemplateType() string
	Subject() string
	To() string
	Bcc() string
	Attachments() []*Attachment
}

type SMSNotifier interface {
	TemplateName() string
	TemplateKeyPairValues() []*TemplateKeyPairValues
	To() string
	From() string
}

type SMSClient interface {
	SendSMS(body, to, from string) error
}
