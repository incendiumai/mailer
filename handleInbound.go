package mailer

import (
	"fmt"
	"net/http"
	"time"

	"github.com/mewis/sendgrid-go/helpers/inbound"
)

func (m *mailer) HandleInbound(callback func(subject string, body map[string]string, from string, to string, attachments map[string][]byte)) http.HandlerFunc {
	h := func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("Message recieved into handler at %s", time.Now().String())
		parsedEmail, err := inbound.Parse(r)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		subject := parsedEmail.Headers["Subject"]
		body := parsedEmail.Body
		from := parsedEmail.Headers["From"]
		to := parsedEmail.Headers["To"]
		attachments := map[string][]byte{}
		for filename, contents := range parsedEmail.Attachments {
			attachments[filename] = contents
		}

		callback(subject, body, from, to, attachments)

		w.WriteHeader(http.StatusOK)
		w.Write([]byte("ok"))
	}

	return h
}
