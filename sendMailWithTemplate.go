package mailer

import (
	"fmt"

	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

// Send an email with a sendGrid template
func (m *mailer) SendMailWithTemplate(n TemplateNotifier) error {
	// get the email details, including key pair values to punch in to the template defined at sendGrid
	templateID := n.TemplateName()
	if templateID == "" {
		return fmt.Errorf("templateID must be provided")
	}

	templateContent := n.TemplateKeyPairValues()
	subject := n.Subject()
	toEmail := mail.NewEmail("", n.To())
	toName := n.To()

	fromName := m.mailDefaultFrom
	fromEmail := mail.NewEmail(fromName, m.mailDefaultFrom)

	dynamicContent := createDynamicContentData(templateContent)
	// add subject to dynamic content always
	dynamicContent["subject"] = n.Subject()

	pp := mail.Personalization{
		To: []*mail.Email{
			{
				Name:    toName,
				Address: toEmail.Address,
			}},
		DynamicTemplateData: dynamicContent,
		Subject:             subject,
		From: &mail.Email{
			Name:    fromName,
			Address: fromEmail.Address,
		},
	}

	if n.Bcc() != "" {
		bccEmail := mail.NewEmail("", n.Bcc())
		bccName := n.Bcc()

		pp.BCC = []*mail.Email{
			{
				Name:    bccName,
				Address: bccEmail.Address,
			}}
	}

	personalisations := []*mail.Personalization{
		&pp,
	}
	body := fmt.Sprintf(`"from": {
		"email": "%s",
			"name": "%s"
	},
	"reply_to": {
		"email": "%s",
			"name": "%s"
	},
	"template_id": "%s"`, fromEmail, fromName, fromEmail, fromName, templateID)

	options := MailOptions{
		FromMailOption:    m.mailDefaultFrom,
		ToMailOption:      toEmail.Address,
		SubjectMailOption: subject,
		BodyMailOption:    body,
		TemplateID:        templateID,
	}
	attachments, err := parseAttachments(n)
	if err != nil {
		return err
	}
	if err := m.sendMail(options, personalisations, attachments); err != nil {
		return fmt.Errorf("calling m.SendMail(), err: %v", err)
	}

	return nil
}

func createDynamicContentData(customContent []*TemplateKeyPairValues) map[string]interface{} {
	dynamicTemplateData := map[string]interface{}{}
	for _, data := range customContent {
		dynamicTemplateData[data.Name] = data.Content
	}

	return dynamicTemplateData
}
