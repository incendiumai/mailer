package mailer

import (
	"bytes"
	"fmt"

	"html/template"
)

func ReadTemplate(data Notifier) (string, error) {
	funcMap := template.FuncMap{
		"safe": func(s string) template.URL {
			return template.URL(s)
		},
	}

	tplBuffer := bytes.Buffer{}
	path :=TemplatePath
	err := template.Must(template.New(data.TemplateName()).
		Funcs(funcMap).
		ParseFiles(fmt.Sprintf("%s/%s/%s", path, data.TemplateType(), data.TemplateName()))).
		Execute(&tplBuffer, data)
	return string(tplBuffer.Bytes()), err
}
