package mailer

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"path"

	"github.com/h2non/filetype"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

func parseAttachments(m Notifier) (attachments []*mail.Attachment, err error) {
	for _, attachment := range m.Attachments() {
		a := mail.NewAttachment()
		buf := attachment.Bytes
		if attachment.Bytes == nil {
			buf, err = ioutil.ReadFile(attachment.Path)
			if err != nil {
				return nil, fmt.Errorf("error reading file: %s", err)
			}
		}
		encoded := base64.StdEncoding.EncodeToString(buf)
		kind, _ := filetype.Match(buf)
		if kind == filetype.Unknown {
			return nil, fmt.Errorf("unknown file type")
		}
		a.SetContent(encoded)
		a.SetType(kind.MIME.Value)
		a.SetFilename(path.Base(attachment.Path))
		a.SetDisposition("attachment")
		attachments = append(attachments, a)
	}
	return
}
