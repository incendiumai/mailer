package mailer

import (
	"github.com/sendgrid/sendgrid-go"
	"testing"
)

func Test_mailer_FetchSendGridTemplates(t *testing.T) {
	SendgridAPIKeyForTest := "enter-your-sendgrid-api-key-here"
	type fields struct {
		client    *sendgrid.Client
		smsClient SMSClient
	}
	type args struct {
		apiKey string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []Template
		wantErr bool
	}{
		{
			name: "Expected Templates Returned",
			fields: fields{
				client:    sendgrid.NewSendClient(SendgridAPIKeyForTest),
				smsClient: nil,
			},
			args:    args{apiKey: SendgridAPIKeyForTest},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &mailer{
				client:    tt.fields.client,
				smsClient: tt.fields.smsClient,
			}
			got, err := m.FetchSendGridTemplates(tt.args.apiKey)
			if (err != nil) != tt.wantErr {
				t.Errorf("FetchSendGridTemplates() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) == 0 {
				t.Errorf("No templates returned for given apiKey")
			}
		})
	}
}
