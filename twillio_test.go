package mailer

import "testing"

func Test_twillioClient_SendSMS(t *testing.T) {
	type fields struct {
		accountSID string
		authToken  string
	}
	type args struct {
		body      string
		fromEmail string
		toEmail   string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test 1 - expected, send sms",
			fields: fields{
				accountSID: "",
				authToken:  "",
			}, args: args{
			body:      "this ia  test to gavin",
			fromEmail: "+14782538801",
			toEmail:   "+447812997449"},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tc := &twillioClient{
				accountSID: tt.fields.accountSID,
				authToken:  tt.fields.authToken,
			}
			if err := tc.SendSMS(tt.args.body, tt.args.fromEmail, tt.args.toEmail); (err != nil) != tt.wantErr {
				t.Errorf("SendSMS() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
