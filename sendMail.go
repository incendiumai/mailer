package mailer

import (
	"net/http"

	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

func (x *mailer) SendMail(n Notifier) error {
	template, err := ReadTemplate(n)
	if err != nil {
		return err
	}

	options := MailOptions{
		FromMailOption:    x.mailDefaultFrom,
		ToMailOption:      n.To(),
		SubjectMailOption: n.Subject(),
		BodyMailOption:    template,
	}

	attachments, err := parseAttachments(n)
	if err != nil {
		return err
	}
	return x.sendMail(options, nil, attachments)
}

func (x *mailer) sendMail(options MailOptions, personalisations []*mail.Personalization, attachments []*mail.Attachment) error {
	fromName := options.GetKey(FromMailOption)
	from := mail.NewEmail(fromName, options.GetKey(FromMailOption))
	subject := options.GetKey(SubjectMailOption)
	to := mail.NewEmail("", options.GetKey(ToMailOption))
	plainTextContent := options.GetKey(BodyMailOption)
	htmlContent := options.GetKey(BodyMailOption)
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	message = message.AddAttachment(attachments...)

	if options.GetKey(TemplateID) != "" {
		message.Personalizations = personalisations
		message.TemplateID = options.GetKey(TemplateID)
	}

	response, err := x.client.Send(message)
	if err != nil {
		return err
	}

	if response.StatusCode != http.StatusAccepted && response.StatusCode != http.StatusOK {
		e, err := NewSengridErrResponse(from.Address, to.Address, subject, response.Body)
		if err != nil {
			return err
		}

		return e
	}

	return nil
}
