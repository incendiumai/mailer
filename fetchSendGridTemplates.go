package mailer

import (
	"fmt"
	"net/http"

	_ "github.com/sendgrid/sendgrid-go/helpers/inbound"

	"encoding/json"
)

const TemplatesListDynamic = `https://api.sendgrid.com/v3/templates?generations=dynamic`

type SendGridGetTemplatesReponse struct {
	Templates []Template `json:"templates"`
}

type Template struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func (m *mailer) FetchSendGridTemplates(apiKey string) ([]Template, error) {
	// set the auth header
	authValue := fmt.Sprintf("Bearer %v", apiKey)
	headers := map[string]string{
		"Authorization": authValue,
	}
	respRaw, err := m.MakeApiRequestGET(TemplatesListDynamic, headers, http.StatusOK)
	if err != nil {
		return nil, err
	}
	templatesResponseJSON := &SendGridGetTemplatesReponse{}
	errJ := json.Unmarshal(respRaw, templatesResponseJSON)
	if errJ != nil {
		return nil, err
	}

	messageTemplates := []Template{}
	for _, t := range templatesResponseJSON.Templates {
		mtNew := Template{ID: t.ID, Name: t.Name}
		messageTemplates = append(messageTemplates, mtNew)
	}

	return messageTemplates, nil
}
