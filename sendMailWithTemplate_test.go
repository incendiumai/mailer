package mailer

import (
	"testing"

	"github.com/sendgrid/sendgrid-go"
)

var TestAPIKeySendGrid = "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"

func Test_mailer_SendMailWithTemplate(t *testing.T) {
	testTNotifier := NewTemplateNotifierTest()
	type fields struct {
		client *sendgrid.Client
	}
	type args struct {
		n TemplateNotifier
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test one - expected results",
			fields: fields{
				client: sendgrid.NewSendClient(TestAPIKeySendGrid),
			},
			args: args{n: testTNotifier},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &mailer{
				client: tt.fields.client}
			if err := m.SendMailWithTemplate(tt.args.n); (err != nil) != tt.wantErr {
				t.Errorf("SendMailWithTemplate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

type testTemplateNotifier struct {
}

func (t testTemplateNotifier) ToName() string {
	return "gavin marriott"
}

func (t testTemplateNotifier) From() string {
	return "gavin@gavinmarriott.com"
}

func (t testTemplateNotifier) FromName() string {
	return "gavin marriott"
}

func (t testTemplateNotifier) TemplateKeyPairValues() []*TemplateKeyPairValues {
	return []*TemplateKeyPairValues{
		{
			Name:    "name",
			Content: "John Smith",
		}, {
			Name:    "industry",
			Content: "software",
		}, {
			Name:    "phone",
			Content: "07867883336",
		},
	}
}

func (t testTemplateNotifier) ReplyToEmail() string {
	return "gavin@gavinmarriott.com"
}

func (t testTemplateNotifier) ReplyToName() string {
	return "Gavin"
}

func (t testTemplateNotifier) TemplateName() string {
	return "d-64720039338f4c18b14ade4f9ecc7a93"
}

func (t testTemplateNotifier) TemplateType() string {
	return "dynamic"
}

func (t testTemplateNotifier) Subject() string {
	return "Welcome Test"
}

func (t testTemplateNotifier) To() string {
	return "gavin@gavinmarriott.com"
}

func (t testTemplateNotifier) Attachments() []*Attachment {
	return nil
}

func NewTemplateNotifierTest() TemplateNotifier {
	return testTemplateNotifier{}
}
