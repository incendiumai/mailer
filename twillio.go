package mailer

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

type twillioClient struct {
	accountSID string
	authToken  string
}

var TwillioSMSEndpointURI = "https://api.twilio.com/2010-04-01/Accounts/%v/Messages"

func NewTwillioClient(accountSID, authToken string) SMSClient {
	return &twillioClient{
		accountSID: accountSID,
		authToken:  authToken,
	}
}

func (tc *twillioClient) SendSMS(body, fromTel, toTel string) error {
	smsBody := body

	msgData := url.Values{}
	msgData.Set("To", toTel)
	msgData.Set("From", fromTel)
	msgData.Set("Body", smsBody)
	msgDataReader := *strings.NewReader(msgData.Encode())

	urlStr := fmt.Sprintf(TwillioSMSEndpointURI, tc.accountSID)

	client := &http.Client{}
	req, err := http.NewRequest("POST", urlStr, &msgDataReader)
	if err != nil {
		return fmt.Errorf("calling http.NewRequest(), err: %v", err)
	}
	req.SetBasicAuth(tc.accountSID, tc.authToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if resp == nil || err != nil {
		return fmt.Errorf("failed executing request post request with client.err: %v", err)
	}

	if resp.StatusCode != 201 {
		var data map[string]interface{}
		decoder := json.NewDecoder(resp.Body)
		err := decoder.Decode(&data)
		if err != nil {
			return fmt.Errorf("decoding data from body request on a failed sms message, err:%v", err)
		}
		return fmt.Errorf("statusCode (%v) from post request when sending SMS was not 201, response(%v)", resp.StatusCode, data)
	}

	return nil
}
