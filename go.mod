module bitbucket.org/incendiumai/mailer

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/h2non/filetype v1.1.3
	github.com/kr/pretty v0.3.0 // indirect
	github.com/mewis/sendgrid-go v3.10.5+incompatible // indirect
	github.com/sendgrid/rest v2.6.9+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.12.0+incompatible
	github.com/stretchr/testify v1.8.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
