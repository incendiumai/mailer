package mailer

import (
	"testing"

	"github.com/sendgrid/sendgrid-go"
)

// test currently failing, todo, resolve
func Test_mailer_SendSMS(t *testing.T) {

	TemplatePath = ""
	clientSMS := NewTwillioClient("", "")
	type fields struct {
		client    *sendgrid.Client
		smsClient SMSClient
	}
	type args struct {
		n SMSNotifier
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "sending message correctly with template",
			fields: fields{
				client:    nil,
				smsClient: clientSMS,
			},
			args:    args{n: smsNotifier{}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &mailer{
				client:    tt.fields.client,
				smsClient: tt.fields.smsClient,
				mailDefaultFrom: "nowhere" ,
			}
			if err := m.SendSMS(tt.args.n); (err != nil) != tt.wantErr {
				t.Errorf("SendSMS() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

type smsNotifier struct {
}

func (s smsNotifier) TemplateName() string {
	return "contact"
}

func (s smsNotifier) TemplateKeyPairValues() []*TemplateKeyPairValues {
	return []*TemplateKeyPairValues{
		{Name: "{name}", Content: "Gavin"},
	}
}

func (s smsNotifier) To() string {
	return "+447812997449"
}

func (s smsNotifier) From() string {
	return "+14782538801"
}

func NewSMSNotifier() SMSNotifier {
	return smsNotifier{}
}
