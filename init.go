package mailer

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/sendgrid/sendgrid-go"
)

var (
	M            Mailer
	TemplatePath = "/modules/notifications/templates/"
)

type ClientSettings struct {
	TwillioAccountSID string
	TwillioAuthToken  string
	SendgridAPIKey    string
}

func Init(clientSetting ClientSettings, templatePath string, smsClient SMSClient, from string) {
	if smsClient == nil {
		smsClient = NewTwillioClient(clientSetting.TwillioAccountSID, clientSetting.TwillioAuthToken)
	}
	M = &mailer{
		client:          sendgrid.NewSendClient(clientSetting.SendgridAPIKey),
		smsClient:       smsClient,
		mailDefaultFrom: from,
	}

	TemplatePath = templatePath
}

type Mailer interface {
	SendMail(m Notifier) error
	SendMailWithTemplate(TemplateNotifier) error
	SendSMS(m SMSNotifier) error
	HandleInbound(callback func(subject string, body map[string]string, from string, to string, attachments map[string][]byte)) http.HandlerFunc
	FetchSendGridTemplates(apiKey string) ([]Template, error)
}

type mailer struct {
	client          *sendgrid.Client
	smsClient       SMSClient
	mailDefaultFrom string
}

type TemplateKeyPairValues struct {
	Name    string `json:"name"`
	Content string `json:"content"`
}

type SendgridErrResponse struct {
	Errors            SendgridErrors `json:"errors"`
	from, to, subject string
}

type SendgridError struct {
	Message string `json:"message"`
	Field   string `json:"field"`
}

type SendgridErrors []SendgridError

func (e *SendgridErrResponse) Error() string {
	res := make([]string, 0)

	for _, s := range e.Errors {
		res = append(res, s.Message)
	}

	return fmt.Sprintf("from: %s; to: %s; subject: %s errors: %s", e.from, e.to, e.subject, strings.Join(res, "; "))
}

func NewSengridErrResponse(from, to, subject, errors string) (*SendgridErrResponse, error) {
	var e SendgridErrResponse

	if err := json.Unmarshal([]byte(errors), &e); err != nil {
		return nil, err
	}

	e.from, e.to, e.subject = from, to, subject

	return &e, nil
}
