package mailer

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

func (m *mailer) MakeApiRequestGET(getURL string, headers map[string]string, codeExpected int) ([]byte, error) {
	r, err := http.NewRequest("GET", getURL, nil)
	r.Header.Set("Content-Type", "application/json")

	if headers != nil {
		for key, value := range headers {
			r.Header.Set(key, value)
		}
	}
	client := &http.Client{Timeout: time.Second * 30}
	resp, err := client.Do(r)
	if err != nil {
		return nil, errors.New("failed to run the GET http request: "+err.Error())
	}

	var respBody []byte
	if resp.Body != nil {
		respBody, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			respBody = []byte{}
		}
	}

	if resp.StatusCode != codeExpected {
		err := errors.New(fmt.Sprintf("wrong response. expected %v but got %v, [body] = %v", codeExpected, resp.StatusCode, string(respBody)))
		return respBody, err
	}

	return respBody, nil
}