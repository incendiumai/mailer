# README

This library has been developed for internal use, feel free to use it but it can change at any time.

### What is this repository for?

- This library provides functionality to send and receive messages using third party providers such as Twillio and
  Sendgrid.
- Mandatory credentials (a valid api key) for SendGrid are required, but the SMS client can be from any provider as long as
  it implements the SMSClient interface. We have Twillio as the default sms client. Pass your custom sms services in the Init() function if you want to use it for all sms messages.
- The handling of receiving emails is done by sendgrid by calling HandleInbound() with your own function (closure function) that processes
  the received email. To learn about how to set up email forwarding in sendGrid visit: https://sendgrid.com/docs/for-developers/parsing-email/setting-up-the-inbound-parse-webhook/

### How do I get set up?

- Import this library into your app using the command: go get https://bitbucket.org/incendiumai/mailer
- Make sure all dependencies are installed with the command: go get ./
- No database is required for using this app.
- To run each test make sure your own credentials are entered in the relevant areas.
  s

### Who do I talk to?

You can create issues if you have any problems and I will try get back to you
